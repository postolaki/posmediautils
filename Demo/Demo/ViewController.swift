import UIKit
import AVKit
import POSMediaUtils

class ViewController: UIViewController {
    
    @IBOutlet weak var originalImageView: UIImageView!
    @IBOutlet weak var resizedImageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        testResizeImage()
//        testPortraitVideo()
//        testLandscapeVideo()
//        testResizeVideoByQuality()
    }
    
    private func testResizeImage() {
        let image = UIImage(named: "image")
        originalImageView.image = image
        
        let resizedWidthImage = image?.resize(newWidth: 100)
        resizedImageView.image = resizedWidthImage
    }
    
    private func testResizeVideoByQuality() {
        let portraitAsset = self.asset(name: "IMG_0251")
        portraitAsset.resize(quality: .low) { _ in }
        portraitAsset.resize(quality: .medium) { _ in }
        portraitAsset.resize(quality: .high) { _ in }
        
        let landscapeAsset = self.asset(name: "IMG_0253")
        landscapeAsset.resize(quality: .low) { _ in }
        landscapeAsset.resize(quality: .medium) { _ in }
        landscapeAsset.resize(quality: .high) { _ in }
        
        portraitAsset.resize(newWidth: 100) { result in
            
        }
        portraitAsset.resize(newHeight: 100) { result in
            
        }
    }
    
    private func testPortraitVideo() {
        let asset = self.asset(name: "IMG_0251")
        print(asset.size)
        asset.resize(newWidth: 414) { result in
            switch result {
            case .success(let asset, let url, let data):
                print(data.sizeKB, asset.size)
            case .failure(let error):
                print(error)
            }
        }
        asset.resize(newHeight: 600) { result in
            switch result {
            case .success(let asset, let url, let data):
                print(url)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func testLandscapeVideo() {
        let asset = self.asset(name: "IMG_0253")
        asset.resize(newWidth: 414) { result in
            switch result {
            case .success(let asset, let url, let data):
                print(url)
            case .failure(let error):
                print(error)
            }
        }
        asset.resize(newHeight: 600) { result in
            switch result {
            case .success(let asset, let url, let data):
                print(url)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func asset(name: String) -> AVAsset {
        guard let videoUrl = Bundle.main.url(forResource: name, withExtension: "MOV") else {
            fatalError("video not found")
        }
        return AVAsset(url: videoUrl)
    }
}
