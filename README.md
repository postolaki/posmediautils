# POSMediaUtils

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements
- [x] Swift 5
- [x] iOS 10 or higher

## Installation

POSMediaUtils is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

``` ruby
pod 'POSMediaUtils'
```

``` swift
import POSMediaUtils

// image resize
let image = UIImage(named: "image")
let resizedWidthImage = image?.resize(newWidth: 100)
or
let resizedWidthImage = image?.resize(newHeight: 100)
        
// video resize
 let videoUrl = Bundle.main.url(forResource: "name", withExtension: "MOV")
 let asset = AVAsset(url: videoUrl)
 
asset.resize(quality: .high) { result in
    switch result {
    case .success(let asset, let url, let data)
        break
    case .failure(let error):
        print(error)
    }
}

asset.resize(newWidth: 100) { result in
    switch result {
    case .success(let asset, let url, let data)
        break
    case .failure(let error):
        print(error)
    }
}

asset.resize(newHeight: 100) { result in
    switch result {
    case .success(let asset, let url, let data)
        break
    case .failure(let error):
        print(error)
    }
}
```

## ResizeQuality
``` swift
enum ResizeQuality: CGFloat {
    /// resize to height 480
    case low = 480
    /// resize to height 720
    case medium = 720
    /// resize to height 1080
    case high = 1080
}
```

## Author
ivan.postolaki@gmail.com

## License
POSMediaUtils is available under the MIT license.