import Foundation
import UIKit
import Accelerate

public extension UIImage {
    func resize(newWidth: CGFloat) -> UIImage? {
        let scale  = newWidth / size.width
        let scaledSize = size.scaledSize(scale: scale)
        
        return resize(to: scaledSize)
    }
    
    func resize(newHeight: CGFloat) -> UIImage? {
        let scale  = newHeight / size.height
        let scaledSize = size.scaledSize(scale: scale)
        
        return resize(to: scaledSize)
    }
    
    /// size in kb
    var sizeKB: CGFloat {
        guard let data = self.pngData() else { return 0 }
        return CGFloat(data.count) / 1024
    }
}

extension UIImage {
    private func resize(to size: CGSize) -> UIImage? {
        guard let cgImage = self.cgImage else { return nil }
        var format = vImage_CGImageFormat(bitsPerComponent: 8,
                                          bitsPerPixel: 32,
                                          colorSpace: nil,
                                          bitmapInfo: CGBitmapInfo(rawValue: CGImageAlphaInfo.first.rawValue),
                                          version: 0,
                                          decode: nil,
                                          renderingIntent: .defaultIntent)
        var sourceBuffer = vImage_Buffer()
        defer {
            free(sourceBuffer.data)
        }
        var error = vImageBuffer_InitWithCGImage(&sourceBuffer, &format, nil, cgImage, numericCast(kvImageNoFlags))
        guard error == kvImageNoError else { return nil }
        // create a destination buffer
        let destWidth = Int(size.width)
        let destHeight = Int(size.height)
        let bytesPerPixel = cgImage.bitsPerPixel / 8
        let destBytesPerRow = destWidth * bytesPerPixel
        let destData = UnsafeMutablePointer<UInt8>.allocate(capacity: destHeight * destBytesPerRow)
        defer {
            destData.deallocate()
        }
        var destBuffer = vImage_Buffer(data: destData,
                                       height: vImagePixelCount(destHeight),
                                       width: vImagePixelCount(destWidth),
                                       rowBytes: destBytesPerRow)
        // scale the image
        error = vImageScale_ARGB8888(&sourceBuffer, &destBuffer, nil, numericCast(kvImageHighQualityResampling))
        guard error == kvImageNoError else { return nil }
        // create a CGImage from vImage_Buffer
        var destCGImage = vImageCreateCGImageFromBuffer(&destBuffer, &format, nil, nil, numericCast(kvImageNoFlags), &error)?.takeRetainedValue()
        guard error == kvImageNoError else { return nil }
        // create a UIImage
        
        let resizedImage = destCGImage.flatMap { UIImage(cgImage: $0, scale: 0.0, orientation: self.imageOrientation) }
        destCGImage = nil
        return resizedImage
    }
}

extension CGSize {
    internal func scaledSize(scale: CGFloat) -> CGSize {
        let scaledSize = CGSize(width: scale * width,
                                height: scale * height)
        return scaledSize
    }
}
