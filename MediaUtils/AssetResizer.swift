import Foundation
import AVKit

final class AssetResizer {
    private let asset: AVAsset
    private let transform: CGAffineTransform
    private let newSize: CGSize
    
    private init() {fatalError()}
    
    init(asset: AVAsset, transform: CGAffineTransform, newSize: CGSize) {
        self.asset = asset
        self.transform = transform
        self.newSize = newSize
    }
    
    func resizeVideo(completion: @escaping AVAsset.CompressCompletion) {
        let assetComposition = AVMutableComposition()
        guard let videoCompositionTrack = assetComposition.addMutableTrack(withMediaType: .video,
                                                                           preferredTrackID: kCMPersistentTrackID_Invalid) else {
                                                                            completion(.failure(.compositionTrack))
                                                                            return
        }
        
        let layerInstructions = AVMutableVideoCompositionLayerInstruction(assetTrack: videoCompositionTrack)
        layerInstructions.setTransform(transform, at: .zero)
        
        let mainInstructions = AVMutableVideoCompositionInstruction()
        mainInstructions.layerInstructions = [layerInstructions]
        mainInstructions.timeRange = CMTimeRange(start: .zero, duration: asset.duration)
        
        let videoComposition = AVMutableVideoComposition()
        videoComposition.renderSize = newSize
        videoComposition.instructions = [mainInstructions]
        videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        
        guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality) else {
            completion(.failure(.exportSession))
            return
        }
        exportSession.outputURL = outputURL
        exportSession.outputFileType = .mov
        exportSession.videoComposition = videoComposition
        
        exportSession.exportAsynchronously {
            switch exportSession.status {
            case .completed:
                guard let outputURL = exportSession.outputURL else {
                    completion(.failure(.invalidOutputURL))
                    return
                }
                guard let data = try? Data(contentsOf: outputURL) else {
                    completion(.failure(.invalidOutputData))
                    return
                }
                let asset = AVAsset(url: outputURL)
                completion(.success(asset, outputURL, data))
            case .failed:
                completion(.failure(.exportSession))
            default:
                break
            }
        }
    }
    
    private lazy var outputURL: URL? = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let timestamp = Date().timeIntervalSince1970
        let timestampString = "\(timestamp)".replacingOccurrences(of: ".", with: "")
        let url = urls.last?.appendingPathComponent(timestampString).appendingPathExtension("mov")
        return url
    }()
}
