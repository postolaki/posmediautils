import Foundation
import AVKit

public extension AVAsset {
    typealias CompressCompletion = (Result) -> Void
    
    enum Result {
        case success(AVAsset, URL, Data)
        case failure(ResizeError)
    }
    
    enum ResizeQuality: CGFloat {
        /// resize to height 480
        case low = 480
        /// resize to height 720
        case medium = 720
        /// resize to height 1080
        case high = 1080
    }
    
    enum ResizeError: Error {
        case trackNotFound
        case compositionTrack
        case exportSession
        case invalidOutputURL
        case invalidOutputData
        case unknown
    }
    
    func resize(newWidth: CGFloat, completion: @escaping CompressCompletion) {
        guard let track = tracks(withMediaType: .video).first else {
            completion(.failure(.trackNotFound))
            return
        }
        
        let scale = newWidth / track.naturalSize.height
        let newSize: CGSize
        var transform = CGAffineTransform(scaleX: scale, y: scale)
        if !isLandscape {
            newSize = CGSize(width: newWidth, height: scale * track.naturalSize.width)
            transform = transform.rotated(by: CGFloat.pi / 2)
            transform = transform.translatedBy(x: 0, y: -track.naturalSize.height)
        } else {
            newSize = CGSize(width: scale * track.naturalSize.width, height: newWidth)
        }
        
        let assetResizer = AssetResizer(asset: self, transform: transform, newSize: newSize)
        assetResizer.resizeVideo(completion: completion)
    }
    
    func resize(newHeight: CGFloat, completion: @escaping CompressCompletion) {
        guard let track = tracks(withMediaType: .video).first else {
            completion(.failure(.trackNotFound))
            return
        }
        
        let scale = newHeight / track.naturalSize.width
        let newSize: CGSize
        var transform = CGAffineTransform(scaleX: scale, y: scale)
        if !isLandscape {
            newSize = CGSize(width: scale * track.naturalSize.height, height: newHeight)
            transform = transform.rotated(by: CGFloat.pi / 2)
            transform = transform.translatedBy(x: 0, y: -track.naturalSize.height)
        } else {
            newSize = CGSize(width: newHeight, height: scale * track.naturalSize.height)
        }
        
        let assetResizer = AssetResizer(asset: self, transform: transform, newSize: newSize)
        assetResizer.resizeVideo(completion: completion)
    }
    
    func resize(quality: ResizeQuality, completion: @escaping CompressCompletion) {
        resize(newHeight: quality.rawValue, completion: completion)
    }
    
    /// asset video track natural size
    var size: CGSize {
        guard let track = tracks(withMediaType: .video).first else { return .zero }
        return track.naturalSize
    }
    
    private var isLandscape: Bool {
        guard let track = tracks(withMediaType: .video).first else { return false }
        let txf = track.preferredTransform
        let size = track.naturalSize
        return size.width == txf.tx && size.height == txf.ty || txf.tx == 0 && txf.ty == 0
    }
}

public extension Data {
    /// size in kb
    var sizeKB: CGFloat {
        return CGFloat(count) / 1024.0
    }
}
