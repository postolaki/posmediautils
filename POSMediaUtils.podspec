Pod::Spec.new do |s|
  s.platform = :ios
  s.ios.deployment_target = '10.0'
  s.name = "POSMediaUtils"
  s.summary = "POSMediaUtils"
  s.requires_arc = true

  s.version = "1.0"
  s.license = { :type => "MIT", :file => "LICENSE" }
  s.author  = { "Ivan" => "ivan.postolaki@gmail.com" }
  s.homepage = "https://bitbucket.org/postolaki/posmediautils/src/master/"
  s.source = { :git => "https://postolaki@bitbucket.org/postolaki/posmediautils.git", :tag => "v" + s.version.to_s }

  s.swift_version = "5"
  s.frameworks = "Foundation", "UIKit"

  s.source_files = "MediaUtils/**/*.{swift}"
end
